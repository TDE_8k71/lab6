package sample;

import Logic.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.event.ActionEvent;


import javax.swing.*;
import java.awt.event.ActionListener;

public class Add {

    public void AddObject(Division division, ObservableList names) {

        JTextField nameField = new JTextField();
        JTextField SharpField = new JTextField();
        JTextField LengthField = new JTextField();
        JTextField SpeedField = new JTextField();
        JTextField DistanceField = new JTextField();
        JTextField PatronField = new JTextField();
        JTextField CaliberField = new JTextField();
        JTextField MagazineField = new JTextField();
        SpeedField.setEditable(false);
        DistanceField.setEditable(false);
        PatronField.setEditable(false);
        CaliberField.setEditable(false);
        MagazineField.setEditable(false);

        ButtonGroup group = new ButtonGroup();
        JRadioButton bladeR = new JRadioButton("Blade", true);
        group.add(bladeR);
        JRadioButton sniperR = new JRadioButton("Sniper", false);
        group.add(sniperR);
        JRadioButton shotgunR = new JRadioButton("ShotGun", false);
        group.add(shotgunR);


        bladeR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                SharpField.setEditable(true);
                LengthField.setEditable(true);
                SpeedField.setEditable(false);
                DistanceField.setEditable(false);
                PatronField.setEditable(false);
                CaliberField.setEditable(false);
                MagazineField.setEditable(false);

            }
        });
        sniperR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                SharpField.setEditable(false);
                LengthField.setEditable(false);
                SpeedField.setEditable(true);
                DistanceField.setEditable(true);
                PatronField.setEditable(true);
                CaliberField.setEditable(false);
                MagazineField.setEditable(true);
            }
        });
        shotgunR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                SharpField.setEditable(false);
                LengthField.setEditable(false);
                SpeedField.setEditable(true);
                DistanceField.setEditable(true);
                PatronField.setEditable(true);
                CaliberField.setEditable(true);
                MagazineField.setEditable(false);
            }
        });
        Object[] message = new Object[]{"", bladeR, "", sniperR, "", shotgunR, "Name", nameField,
                "Sharp", SharpField, "Length", LengthField, "Speed", SpeedField, "Distance", DistanceField, "Patron"
                , PatronField, "Caliber", CaliberField, "Magazine", MagazineField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Create new weapon", JOptionPane.OK_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            String name = nameField.getText();

            if (bladeR.isSelected()) {
                int sharp = Integer.parseInt(SharpField.getText());
                int length = Integer.parseInt(LengthField.getText());
                BladeWeapon blade = new BladeWeapon(name, sharp, length);
                division.AddWeapon(blade);
                names.add("Blade: " + blade.GetName() + "; Cost: " + Integer.toString(blade.GetCost()));
            }
            if (sniperR.isSelected()) {
                int speed = Integer.parseInt(SpeedField.getText());
                int distance = Integer.parseInt(DistanceField.getText());
                int patron = Integer.parseInt(PatronField.getText());
                int magazine = Integer.parseInt(MagazineField.getText());
                Sniper sniper = new Sniper(name, speed, distance, patron, magazine);
                division.AddWeapon(sniper);
                names.add("Sniper: " + sniper.GetName() + "; Cost: " + Integer.toString(sniper.GetCost()));
            }
            if (shotgunR.isSelected()) {
                int speed = Integer.parseInt(SpeedField.getText());
                int distance = Integer.parseInt(DistanceField.getText());
                int patron = Integer.parseInt(PatronField.getText());
                int caliber = Integer.parseInt(CaliberField.getText());
                ShotGun shotGun = new ShotGun(name, speed, distance, patron, caliber);
                division.AddWeapon(shotGun);
                names.add("ShotGun: " + shotGun.GetName() + "; Cost: " + Integer.toString(shotGun.GetCost()));
            }

        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");

        }
    }

}
