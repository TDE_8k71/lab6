package sample;

import Logic.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


import javax.swing.*;
import java.awt.event.ActionListener;

public class Controller {
    public Button bladebtn;
    public Label Pricelbl;
    public Button editbtn;
    public Button deletebtn;
    Add add = new Add();
    Edit edit = new Edit();

    Division division = new Division();
    public ListView<String> list = new ListView<>();
    public ObservableList<String> names = FXCollections.observableArrayList();

    public void AddButton(ActionEvent event) {
        add.AddObject(division, names);
        list.setItems(names);
        Pricelbl.setText(Integer.toString(division.GetPrice()));
    }


    public void EditButton(ActionEvent event) {
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();

            if (division.GetArrayList().get(index).getClass().equals(BladeWeapon.class)) {
                BladeWeapon bladeWeapon = (BladeWeapon) division.GetArrayList().get(index);
                edit.EditAct(division, bladeWeapon, list, names, index);
            }
            if (division.GetArrayList().get(index).getClass().equals(Sniper.class)) {
                Sniper sniper = (Sniper) division.GetArrayList().get(index);
                edit.EditAct(division, sniper, list, names, index);
            }
            if (division.GetArrayList().get(index).getClass().equals(ShotGun.class)) {
                ShotGun shotGun = (ShotGun) division.GetArrayList().get(index);
                edit.EditAct(division, shotGun, list, names, index);
            }

            Pricelbl.setText(Integer.toString(division.GetPrice()));
        } catch (Exception e) {
            new Extentions().showAlertWithoutHeaderText("Please, choose any weapon!");
        }
    }


    public void DeleteButton(ActionEvent event) {
        try {
            int index;
            index = list.getSelectionModel().getSelectedIndex();
            division.GetArrayList().remove(index);
            names.remove(index);
            Pricelbl.setText(Integer.toString(division.GetPrice()));
        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Please, choose any weapon!");
        }
    }
}





