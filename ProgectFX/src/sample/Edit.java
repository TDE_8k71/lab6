package sample;

import Logic.BladeWeapon;
import Logic.Division;
import Logic.ShotGun;
import Logic.Sniper;
import javafx.collections.ObservableList;

import javax.swing.*;


public class Edit {

    public void  EditAct(Division division, BladeWeapon bladeWeapon, javafx.scene.control.ListView list, ObservableList names, int index){

        JTextField nameField = new JTextField(bladeWeapon.GetName());
        JTextField SharpField = new JTextField(Integer.toString(bladeWeapon.GetSharp()));
        JTextField LengthField = new JTextField(Integer.toString(bladeWeapon.GetLength()));

        Object[] message = new Object[]{ "Name", nameField,
                "Sharp", SharpField, "Length", LengthField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            bladeWeapon.SetName(nameField.getText());
            bladeWeapon.SetCost(Integer.parseInt(LengthField.getText(),Integer.parseInt(SharpField.getText())));
            bladeWeapon.SetSharp(Integer.parseInt(SharpField.getText()));
            bladeWeapon.SetLength(Integer.parseInt(LengthField.getText()));

            list.setItems(names);
            names.set(index, "Blade: " + bladeWeapon.GetName() + "; Cost: " + Integer.toString(bladeWeapon.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }


    }
    public void EditAct(Division division, Sniper sniper, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(sniper.GetName());
        JTextField SpeedField = new JTextField(Integer.toString(sniper.GetSpeed()));
        JTextField DistanceField = new JTextField(Integer.toString(sniper.GetDistance()));
        JTextField PatronField = new JTextField(Integer.toString(sniper.GetPatron()));
        JTextField MagazineField = new JTextField(Integer.toString(sniper.GetMagazine()));

        Object[] message = new Object[]{ "Name", nameField,
                "Speed", SpeedField, "Distance", DistanceField, "Patron", PatronField,"Magazine", MagazineField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            sniper.SetName(nameField.getText());
            sniper.SetCost(Integer.parseInt(MagazineField.getText()));
            sniper.SetSpeed(Integer.parseInt(SpeedField.getText()));
            sniper.SetDistance(Integer.parseInt(DistanceField.getText()));
            sniper.SetPatron(Integer.parseInt(PatronField.getText()));
            sniper.SetMagazine(Integer.parseInt(MagazineField.getText()));

            list.setItems(names);
            names.set(index, "Sniper: " + sniper.GetName() + "; Cost: " + Integer.toString(sniper.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }
    }
    public void EditAct(Division division, ShotGun shotGun, javafx.scene.control.ListView list, ObservableList names, int index){
        JTextField nameField = new JTextField(shotGun.GetName());
        JTextField SpeedField = new JTextField(Integer.toString(shotGun.GetSpeed()));
        JTextField DistanceField = new JTextField(Integer.toString(shotGun.GetDistance()));
        JTextField PatronField = new JTextField(Integer.toString(shotGun.GetPatron()));
        JTextField CaliberField = new JTextField(Integer.toString(shotGun.GetCaliber()));

        Object[] message = new Object[]{ "Name", nameField,
                "Speed", SpeedField, "Distance", DistanceField, "Patron", PatronField,"Caliber", CaliberField};
        int option = JOptionPane.showConfirmDialog(null, message,
                "Edit Weapon", JOptionPane.OK_CANCEL_OPTION);
        if (option != JOptionPane.OK_OPTION) return;
        try {
            shotGun.SetName(nameField.getText());
            shotGun.SetCost(Integer.parseInt(CaliberField.getText()));
            shotGun.SetSpeed(Integer.parseInt(SpeedField.getText()));
            shotGun.SetDistance(Integer.parseInt(DistanceField.getText()));
            shotGun.SetPatron(Integer.parseInt(PatronField.getText()));
            shotGun.SetCaliber(Integer.parseInt(CaliberField.getText()));

            list.setItems(names);
            names.set(index, "ShotGun: " + shotGun.GetName() + "; Cost: " + Integer.toString(shotGun.GetCost()));


        } catch (Exception exc) {
            new Extentions().showAlertWithoutHeaderText("Enter correct values, please!");
        }
    }


}

